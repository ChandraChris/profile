<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">
      <hr>
      <?php foreach ($blog->result() as $key): ?>
        <div class="post-preview">
          <h2 class="post-title">
            <?php echo $key->judul;?>
          </h2>
          <p style="font-size: 24px;" class="post-subtitle">
            <?php 

            $artikel = $key->isi;
            $cut = substr($artikel,0, 100);
            echo $cut;?> <a style="text-decoration: none;" href="<?php echo base_url('home/readmore/' . $key->id);?>"><small style="font-size: 15px;"> Read more....</small></a>
          </p>
          <p class="post-meta">Posted by
            <a href="#"><?php echo $key->penulis;?></a>
            on <?php echo $key->tanggal?></p>
          </div>
          <hr>
        <?php endforeach ?>
        <!-- Pager -->
          <!-- <div class="clearfix">
            <a class="btn btn-primary float-left" href="#">&larr;Add Post</a>
            <a class="btn btn-success float-right" href="#">Older Posts&rarr;</a>
          </div> -->
        </div>
      </div>
    </div>