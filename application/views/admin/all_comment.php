<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Comment</h1>
			<div class="table-responsive">
				<table class="table table-hover table-bordered table-condensed">
					<thead>
						<tr>
							<th>Id</th>
							<th>Nama</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Message</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($komen->result() as $key): ?>
							<tr>
								<td><?php echo $key->id; ?></td>
								<td><?php echo $key->nama; ?></td>
								<td><?php echo $key->email; ?></td>
								<td><?php echo $key->no_telp; ?></td>
								<td><?php echo $key->message; ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>