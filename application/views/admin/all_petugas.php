<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">List Petugas</h1>
			<div class="table-responsive">
				<table class="table table-hover table-bordered table-condensed">
					<thead>
						<tr>
							<th>Nip</th>
							<th>Nama</th>
							<th>Level</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($petugas->result() as $key): ?>
							<tr>
								<td><?php echo $key->nip; ?></td>
								<td><?php echo $key->nama; ?></td>
								<td><?php echo $key->level; ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>