<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">All Blog</h1>
			<div class="table-responsive">
				<table class="table table-hover table-bordered table-condensed">
					<thead>
						<tr>
							<th>ID</th>
							<th>TITLE</th>
							<th>DESCRIPTION</th>
							<th>Creator</th>
							<th>DATE</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($blog->result() as $key): ?>
							<tr>
								<td><b><?php echo $key->id ?></b></td>
								<td><a href="<?php echo base_url('home/readmore/' . $key->id)?>" style="text-decoration: none;"><b><?php echo $key->judul ?></b></a></td>
								<td><?php echo $key->isi ?></td>
								<td class="success"><b><?php echo $key->penulis ?></b></td>
								<td><?php echo $key->tanggal ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>